import * as types from '../../mutation-types'
import lazyLoading from './lazyLoading'

// show: meta.label -> name
// name: component name
// meta.label: display label

const state = {
  items: [
    {
      name: 'Dashboard',
      path: '/dashboard',
      meta: {
        auth: true,
        icon: 'fa-tachometer',
        link: 'dashboard/index.vue'
      },
      component: lazyLoading('dashboard', true)
    },
    {
      name: 'Usuários',
      path: '/users',
      meta: {
        auth: true,
        icon: 'fa-users',
        link: 'user/index.vue'
      },
      component: lazyLoading('user', true)
    },
    {
      name: 'Instituições',
      path: '/institution',
      meta: {
        auth: true,
        icon: 'fa-university',
        link: 'institution/index.vue'
      },
      component: lazyLoading('institution', true)
    },
    {
      name: 'Projetos',
      path: '/project',
      meta: {
        auth: true,
        icon: 'fa-rocket',
        link: 'project/index.vue'
      },
      component: lazyLoading('project', true)
    },
    {
      name: 'Pesquisas',
      path: '/research',
      meta: {
        auth: true,
        icon: 'fa-search',
        link: 'research/index.vue'
      },
      component: lazyLoading('research', true)
    },
    {
      name: 'Produções',
      path: '/production',
      meta: {
        auth: true,
        icon: 'fa-clone',
        link: 'production/index.vue'
      },
      component: lazyLoading('production', true)
    },
    {
      name: 'Notícias',
      path: '/news',
      meta: {
        auth: true,
        icon: 'fa-newspaper-o',
        link: 'news/index.vue'
      },
      component: lazyLoading('news', true)
    },
    {
      name: 'Links Úteis',
      path: '/util-link',
      meta: {
        auth: true,
        icon: 'fa-link',
        link: 'util-link/index.vue'
      },
      component: lazyLoading('util-link', true)
    }
  ]
}

const mutations = {
  [types.EXPAND_MENU] (state, menuItem) {
    if (menuItem.index > -1) {
      if (state.items[menuItem.index] && state.items[menuItem.index].meta) {
        state.items[menuItem.index].meta.expanded = menuItem.expanded
      }
    } else if (menuItem.item && 'expanded' in menuItem.item.meta) {
      menuItem.item.meta.expanded = menuItem.expanded
    }
  }
}

export default {
  state,
  mutations
}
